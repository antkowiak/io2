<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', [
            'label' => 'Tytuł'
        ]);
        $builder->add('note', 'textarea', [
            'label' => 'Notka'
        ]);
    }

    public function getName()
    {
        return 'note';
    }
}
