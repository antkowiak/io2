<?php
namespace AppBundle\Service;

use AppBundle\Entity\Note;
use Doctrine\ORM\EntityManager;

class NoteManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function removeNote(Note $note)
    {
        $this->remove($note);
    }

    public function remove($e)
    {
        $this->em->remove($e);
        $this->em->flush();
    }

    public function getNote($id)
    {
        return $this->getNoteRepository()->find($id);
    }

    public function getNoteRepository()
    {
        return $this->em->getRepository('AppBundle:Note');
    }

    public function getAllNotes()
    {
        return $this->getNoteRepository()->findAll();
    }

    public function save($e)
    {
        $this->em->persist($e);
        $this->em->flush();
    }
}