<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Note;
use AppBundle\Form\NoteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template()
     */
    public function indexAction()
    {
        $noteManager = $this->get('app.note_manager');
        $notes = $noteManager->getAllNotes();


        return ['notes' => $notes];
    }

    /**
     * @Route("/add", name="add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $noteManager = $this->get('app.note_manager');
        $note = new Note();
        $form = $this->createForm(new NoteType(), $note);
        $form->add('Dodaj', 'submit', [
            'attr' => ['class' => 'btn btn-primary']
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $noteManager->save($note);
            $this->addFlash('success', 'Dodano notkę.');

            return $this->redirectToRoute('index');
        }


        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $noteManager = $this->get('app.note_manager');
        $note = $noteManager->getNote($id);
        if (!$note) {
            $this->addFlash('error', 'Notka nie istnieje.');

            return $this->redirectToRoute('index');
        }
        $form = $this->createForm(new NoteType(), $note);
        $form->add('Edytuj', 'submit', [
            'attr' => ['class' => 'btn btn-primary']
        ]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $noteManager->save($note);
            $this->addFlash('success', 'Zedytowano notkę.');

            return $this->redirectToRoute('index');
        }


        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {
        $noteManager = $this->get('app.note_manager');
        $note = $noteManager->getNote($id);
        if (!$note) {
            $this->addFlash('error', 'Notka nie istnieje.');


            return $this->redirectToRoute('index');
        }

        $noteManager->removeNote($note);
        $this->addFlash('success', 'Usunięto notkę.');


        return $this->redirectToRoute('index');
    }
}
